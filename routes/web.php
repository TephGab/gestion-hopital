<?php

use Illuminate\Support\Facades\Route;
use App\Role;
use App\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// $checksuperadmin = Role::where('name', 'superadministrator');
// $checksuperadmin = User::count();
// if ($checksuperadmin == 0) {
//     Route::get('/', function () {
//         return view('login');
//     });
// }
Route::get('/', 'CheckLoginController@index')->name('login');;

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::Resource('user', 'UserController');

Route::Resource('admin', 'AdminController');

Route::Resource('manage/users', 'ManageUserController');

Route::Resource('manage/patients', 'ManagePatientController');

Route::Resource('manage/dossiers', 'ManageDossierController');