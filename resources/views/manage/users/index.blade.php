@extends('layouts.app')
@section('content')
<style>
    
/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

/* Change the color of links on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Style the "active" element to highlight the current page */
.topnav a.active {
  background-color: #2196F3;
  color: white;
}

/* Style the search box inside the navigation bar */
.topnav input[type=text] {
  float: right;
  padding: 6px;
  border: none;
  margin-top: 8px;
  margin-right: 16px;
  font-size: 17px;
}

/* When the screen is less than 600px wide, stack the links and the search field vertically instead of horizontally */
@media screen and (max-width: 600px) {
  .topnav a, .topnav input[type=text] {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;
  }
}
</style>     
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Users</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Manage users</li>
                        </ol>
                        <div>
                          <a href="{{ route('users.create') }}" class="btn btn-success"> <i class="fas fa-user-plus"></i> Nouvel Employe</a>

                            {{-- <hr> --}}
                        </div>

                        
                        

                              <div class="table-responsive">
      
                <div class="topnav">
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
                </div> 
        <table class="table table-striped table-sm" id="myTable">
          <thead>
            <tr>
              <th>Photo</th>
              <th>Nom</th>
              <th>Email</th>
              <th>Actions</th>
          {{--     <th>Prenom</th>
              <th>Sexe</th>
              <th>Niveau</th>
              <th>Action</th> --}}
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
                <tr>
                   <td> 
                      <img src="{{ asset('storage/img/users/' . $user->photo) }}" width="50px;" height="50px;" style="border-radius: 50px 50px">

                   </td>

                    @if($user->hasRole('superadministrator'))
                        </td>
                            {{ $user->name }} -Superadmin
                        </td>
                          

                        @elseif($user->hasRole('administrator'))
                            <td>
                                {{ $user->name }} - Admin
                            </td>
                          

                            @elseif($user->hasRole('user'))
                                <td>
                                    {{ $user->name }} - User
                                </td>
                         
                            @else
                                <td>
                                    {{ $user->name }} - Role non defini
                                </td>
                            @endif

                             <td>
                                 {{ $user->email }}
                             </td>

                             <td> 
                                <a href="{{ route('users.show', $user->id) }}" title="Plus de details"><i class="fas fa-eye"></i></a>
                             </td>
                 </tr>  
                        @endforeach
          
              
                {{-- <td>{{ $etudiant['code_etudiant'] }}</td>
                <td> <img src="{{ asset('storage/img/'.$etudiant->photo) }}" class="img img-thumbnail" width="60px;"> </td>
                <td>{{ $etudiant->nom }}</td>
                <td>{{ $etudiant->prenom }}</td>
                <td>{{ $etudiant->sexe }}</td> --}}
               {{--  <td>{{ $etudiant->etudiants->niveau }}</td> --}}
              
             
            
          </tbody>
        </table>
      </div>
                    </div>
                </main> 




                <script>
                  function myFunction() {
                    var input, filter, table, tr, td, i, txtValue;
                    input = document.getElementById("myInput");
                    filter = input.value.toUpperCase();
                    table = document.getElementById("myTable");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                      td = tr[i].getElementsByTagName("td")[1];
                      tdemail = tr[i].getElementsByTagName("td")[2];
                      if (td || tdemail) {
                        txtValue = td.textContent || td.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                          tr[i].style.display = "";
                        } else {
                          tr[i].style.display = "none";
                        }
                      }       
                    }
                  }
                </script>    
@endsection
