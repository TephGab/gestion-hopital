@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{--  --}}
            <div id="smartwizard">
                <ul class="nav">
                   <li>
                     <a class="nav-link" href="#step-1"> Step 1</a>
                   </li>

                   <li>
                     <a class="nav-link" href="#step-2"> Step 2 </a>
                   </li>
                </ul>

                  <div class="tab-content">
                    <div id="step-1" class="tab-pane" role="tabpanel">
                       <form method="POST" id="myForm" action="{{ route('patients.store') }}">
                            <div class="row">
                                <div class="col-md-8">
                                @csrf

                                    <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nom complet') }}</label>

                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            <span class="invalid-feedback" role="alert">
                                              
                                            </span>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                   <div id="step-2" class="tab-pane" role="tabpanel">
                      Step 2
                   </div>

                 </div>
                </div>
            {{--  --}}
        </div>
    </div>
</div>

    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrapvalidator/dist/js/bootstrapValidator.js') }}"></script>
    <script src="{{ asset('plugins/Smart-Wizard/dist/js/jquery.smartWizard.min.js') }}"></script>
       <script>
        $('#smartwizard').smartWizard({
            theme: 'dots',
            transitionEffect: 'slide',
        });

    
        $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
            if(stepDirection === 'forward'){
                var name = document.getElementById("name").value;
                    axios
                        .post("/manage/patients", {
                                name: name
                                })
                                .then(res => {
                                    console.log(res.data);
                                })
                                .catch(err => console.log(err));  
                    // return confirm("Do you want to leave the step " + currentStepIndex + "?");
            }
       
    });
    </script>
@endsection