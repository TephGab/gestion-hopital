<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Gestion hopital" />
    <meta name="author" content="Group Connect-IT" />
    <title>{{ page_title($title ?? '') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('auth/css/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />

    <!--Bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" />

    <!--Font awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-5.14.0/css/all.css') }}">
    <script defer src="{{ asset('plugins/fontawesome-5.14.0/js/all.js') }}"></script>

    <!-- fonts -->
    <link href="{{ asset('plugins/fonts/Quicksand/static/Quicksand-Light.ttf') }}" rel="stylesheet">

    <!-- Core Stylesheet -->
    <link href="{{ asset('plugins/Smart-Wizard/dist/css/smart_wizard.min.css') }}" rel="stylesheet" />
    <!-- All In One -->
    <link href="{{ asset('plugins/Smart-Wizard/dist/css/smart_wizard_all.min.css') }}" rel="stylesheet" />
</head>
<body class="sb-nav-fixed" style="background-color: rgb(236, 240, 245)">
    @include('layouts\partials\_top_nav')
    <div id="layoutSidenav">
        @include('layouts\partials\_side_nav')
        <div id="layoutSidenav_content">
            @yield('content')
            {{-- @include('layouts\partials\_footer') --}}
        </div>
    </div>

    <!-- Scripts -->
      <script src="{{ asset('auth/vendor/select2/select2.min.js') }}"></script>
      <script src="{{ asset('auth/vendor/datepicker/moment.min.js') }}"></script>
      <script src="{{ asset('auth/vendor/datepicker/daterangepicker.js') }}"></script>

      <!-- Main JS-->
      <script src="js/global.js"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    @include('sweetalert::alert')
</body>
</html>
