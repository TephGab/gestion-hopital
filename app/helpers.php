<?php
use App\User;
use App\Departement;

//Page title
if (! function_exists('page_title')) {

    function page_title($title){
        if ($title === '') {
            return  config('app.name');
        }
        else {
            return $title . ' | ' . config('app.name');
        }
    }
}
//end page titile
//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//Active route
if (! function_exists('set_active_route')) {

    function set_active_route($route){
       return Route::is($route) ? 'active' : '';
    }
}
//End active route
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//User counter
if (! function_exists('user_counter')) {

    function user_counter(){
      return $nb_user = User::count();
    }
}
//End user counter
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//Find superadministrator
if (! function_exists('find_superadmin')) {

    function find_superadmin(){
      return $superadmin = User::first();
    }
}
//End
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
if (! function_exists('list_dep')) {
    function list_dep(){
        return $departements = DB::table('departements')->pluck('dep_name');
      // return $departements = Departement::all();
    }
}