<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('superadministrator') || $user->hasRole('administrator')) {
            $user = Auth::user()->name;
            return redirect()->route('admin.index')->with('toast_success','Welcome '."{$user}");
            // return $redirect('/admin');
        }
        
        if ($user->hasRole('user')) {
            return redirect()->route('user.index');
        }
        // alert('Title','Lorem Lorem Lorem', 'success');
    }
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
