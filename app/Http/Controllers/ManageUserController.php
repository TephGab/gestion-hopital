<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class ManageUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('manage.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $roles = Role::all();
         return view('manage.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('photo')) {
        
        $fileNameWithExt = $request->file('photo')->getClientOriginalName();

        $fileName = pathInfo($fileNameWithExt, PATHINFO_FILENAME);

        $extension = $request->file('photo')->getClientOriginalExtension();

        $fileNameToStore = $fileName.'_'.time().'.'.$extension;

        $path = $request->file('photo')->storeAs('public/img/users', $fileNameToStore);
        }

        $user = new User;
        $user->name = request('name');
        $user->email = request('email');
        $user->password = Hash::make(request('password'));
            if (isset($fileNameToStore)) {
                $user->photo = $fileNameToStore;
            }       
        $user->save();

         if (request('role') == 'superadministrator'){
            $user->attachRole('superadministrator');
        }
        if (request('role') == 'administrator'){
            $user->attachRole('administrator');
        }
        if(request('role') == 'medecin'){
            $user->attachRole('medecin');
        }
         if(request('role') == 'infirmier'){
            $user->attachRole('infirmier');
        }
        if(request('role') == 'user'){
            $user->attachRole('user');
        }

        $users = User::all();
        return view('auth.login',);  
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = user::findOrFail($id);
         return view('manage.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = User::findOrFail($id);
        return view('manage.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->Update([
            'name' => $request->name,
            'email' => $request->email,
            // 'sexe' => $request->sexe,
            // 'code_classe' => $request->code_classe,
            // 'photo' => $request->photo,
        ]);

         return redirect()->route('users.show', $id)->with('success', 'Employe modifie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        
        return redirect()->route('users.index');
    }
}
